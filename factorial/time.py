import timeit
from factorial import factorial, factorial_iter


# settings:
set_repeat = 100000
set_range = 20
set_func1 = factorial
set_func2 = factorial_iter


for i in range (1,set_range):
    print (set_func1 (i))

setup1 = '''
from '''+set_func1.__name__+''' import '''+set_func1.__name__+'''
'''
code1 = '''
for i in range (1,'''+str (set_range)+'''):
    '''+set_func1.__name__+''' (i)
'''
t1 = timeit.Timer (setup=setup1, stmt=code1)
t1_min = min (t1.repeat (set_repeat, 1))
print (t1_min)


for i in range (1,set_range):
    print (set_func2 (i))

setup2 = '''
from '''+set_func1.__name__+''' import '''+set_func2.__name__+'''
'''
code2 = '''
for i in range (1,'''+str (set_range)+'''):
    '''+set_func2.__name__+''' (i)
'''
t2 = timeit.Timer (setup=setup2, stmt=code2)
t2_min = min (t2.repeat (set_repeat, 1))
print (t2_min)
