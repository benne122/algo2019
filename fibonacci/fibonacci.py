def fibonacci (n):

    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fibonacci (n-1) + fibonacci (n-2)

def fibonacci_mem_wrap (n):

    global memo
    memo = {0:0, 1:1}
    return fibonacci_mem (n)

def fibonacci_mem (n):
    if not n in memo:
        memo[n] = fibonacci_mem(n-1) + fibonacci_mem(n-2)
    return memo[n]
