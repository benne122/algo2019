import timeit
import matplotlib.pyplot as plt
import numpy
from fibonacci import fibonacci, fibonacci_mem_wrap

# settings:
set_repeat = 100000
set_range = 10
set_func1 = fibonacci
set_func2 = fibonacci_mem_wrap



# "setup" fuer Timer, hier werden die Funktionen erneut importiert. Dreifache Hochstriche fuer Strings ueber mehrere Zeilen.
setup1 = '''
from '''+set_func1.__name__+''' import '''+set_func1.__name__+'''
'''
setup2 = '''
from '''+set_func1.__name__+''' import '''+set_func2.__name__+'''
''' 

x_func = numpy.arange(0.0, set_range, 1)
y_func1 = []
y_func2 = []
delta_func = []


# Schleife fuer die Anzahl "set_range"; berechnet nach und nach von 1 bis set_range die Funktionswerte und gibt sie aus
for i in range (set_range):
    # 


    # zu timender Code fuer Timer1 in Stringform; set_func.__name__ gibt den set_func1 zugewiesenen Funktionsnamen als String zurueck; Zahlen muessen auch als String vorliegen
    code1 = set_func1.__name__ +"("+str (i)+")"
    
    # Initialisierung erste77r Timer
    t1 = timeit.Timer (setup=setup1, stmt=code1)
    # repeat gibt eine Liste aus in der "set_repeat" viele Messungen enthalten sind. Der minimale Wert ist der aussagekraeftigste, da hier der Prozessor am wenigsten gestoert wurde. Die 1 ist die Anzahl pro repeatvorgang, hier werden allerdings die Zeiten auffaddiert, weswegen 1 als Wert gewaehlt ist
    t1_min = min (t1.repeat (set_repeat, 1))
    
    # Ausgabe des Funktionsnamen plus Funktionswert von "i" aus for Schleife
    # print (set_func1.__name__+": "+str (set_func1 (i)))

    # Ausgabe des Funktionsnamen plus minimaler Wert aus repeat. Dies ist die Ausgabe der Messung
    # print ("lokale Laufzeit: "+str (t1_min))

    y_func1.append (t1_min)

    # Wiederholung fuer die zweite Funktion
    code2 = set_func2.__name__ +"("+str (i)+")"
    t2 = timeit.Timer (setup=setup2, stmt=code2)
    t2_min = min (t2.repeat (set_repeat, 1))
    # print (set_func2.__name__+": "+str (set_func2 (i)))
    # print ("lokale Laufzeit: "+str (t2_min))

    y_func2.append (t2_min)

    # Ausgabe des Verhaeltnisses der Laufzeit von Funktion 1 zu Funktion 2 mit Abfangen von Laufzeiten, die so klein sind, dass der Wert 0 entspricht; damit Teilung durch 0 verhindert wird
    # if (t1_min == 0):
    #     print ("t2/t1: t1=0")
    # else:
    #     print ("t2/t1: "+str (t2_min/t1_min))
    # print ("____________________")
    
    if (t1_min == 0.0):
        delta_func.append (0)
    else:
        delta_func.append (t2_min/t1_min)
    
# code für gui plotting mit matplotlib
#   plottet 2 Funktionen in Bezug zu einer y-Achse und das Zeitdelta in Bezug zu einer zweiten y-Achse
fig, ax1 = plt.subplots()

# farben von func1 und func2
color1 = 'tab:red'
color2 = 'tab:blue'
# Achsenbezeichnungen
ax1.set_xlabel ('iterations')
ax1.set_ylabel ('functions')
ax1.plot (x_func, y_func1, color=color1)
ax1.plot (x_func, y_func2, color=color2)

ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

color = 'tab:orange'
ax2.set_ylabel ('func2/func1', color=color)  # we already handled the x-label with ax1
ax2.plot(x_func, delta_func, color=color)
ax2.tick_params(axis='y', labelcolor=color)

fig.tight_layout()  # otherwise the right y-label is slightly clipped
plt.show()
