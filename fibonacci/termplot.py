import timeit
import termplotlib as tpl
import numpy
from fibonacci import fibonacci, fibonacci_mem_wrap

# settings:
set_repeat = 100000
set_range = 8
set_func1 = fibonacci
set_func2 = fibonacci_mem_wrap



# "setup" fuer Timer, hier werden die Funktionen erneut importiert. Dreifache Hochstriche fuer Strings ueber mehrere Zeilen.
setup1 = '''
from '''+set_func1.__name__+''' import '''+set_func1.__name__+'''
'''
setup2 = '''
from '''+set_func1.__name__+''' import '''+set_func2.__name__+'''
''' 

x_func1 = numpy.linspace (1, set_range, 5)
x_func2 = numpy.linspace (1, set_range, 5)
y_func1 = []
y_func2 = []
delta_func = []


# Schleife fuer die Anzahl "set_range"; berechnet nach und nach von 1 bis set_range die Funktionswerte und gibt sie aus
for i in range (set_range):
    # 


    # zu timender Code fuer Timer1 in Stringform; set_func.__name__ gibt den set_func1 zugewiesenen Funktionsnamen als String zurueck; Zahlen muessen auch als String vorliegen
    code1 = set_func1.__name__ +"("+str (i)+")"
    
    # Initialisierung erste77r Timer
    t1 = timeit.Timer (setup=setup1, stmt=code1)
    # repeat gibt eine Liste aus in der "set_repeat" viele Messungen enthalten sind. Der minimale Wert ist der aussagekraeftigste, da hier der Prozessor am wenigsten gestoert wurde. Die 1 ist die Anzahl pro repeatvorgang, hier werden allerdings die Zeiten auffaddiert, weswegen 1 als Wert gewaehlt ist
    t1_min = min (t1.repeat (set_repeat, 1))
    
    # Ausgabe des Funktionsnamen plus Funktionswert von "i" aus for Schleife
    # print (set_func1.__name__+": "+str (set_func1 (i)))

    # Ausgabe des Funktionsnamen plus minimaler Wert aus repeat. Dies ist die Ausgabe der Messung
    # print ("lokale Laufzeit: "+str (t1_min))

    y_func1.append (t1_min)

    # Wiederholung fuer die zweite Funktion
    code2 = set_func2.__name__ +"("+str (i)+")"
    t2 = timeit.Timer (setup=setup2, stmt=code2)
    t2_min = min (t2.repeat (set_repeat, 1))
    # print (set_func2.__name__+": "+str (set_func2 (i)))
    # print ("lokale Laufzeit: "+str (t2_min))

    y_func2.append (t2_min)

    # Ausgabe des Verhaeltnisses der Laufzeit von Funktion 1 zu Funktion 2 mit Abfangen von Laufzeiten, die so klein sind, dass der Wert 0 entspricht; damit Teilung durch 0 verhindert wird
    # if (t1_min == 0):
    #     print ("t2/t1: t1=0")
    # else:
    #     print ("t2/t1: "+str (t2_min/t1_min))
    # print ("____________________")
    
    # Hängt an die Liste delta_func das aktuelle Verhältnis der Laufzeiten von func2 zu func1 an
    #   Ausnahme: Laufzeit von func1 ist 0 --> Fehler: durch null teilen; vereinfacht 0 anhängen
    if (t1_min == 0.0):
        delta_func.append (0)
    else:
        delta_func.append (t2_min/t1_min)
    
# Initialisierung erster Konsolenplot für func1
fig1 = tpl.figure()
fig1.plot (x_func1, y_func1, label=y_func1, width=50, height=30)
fig1.show()

# Initialisierung zweiter Konsolenplot für func2
fig2 = tpl.figure()
fig2.plot (x_func2, y_func2, label=y_func2, width=50, height=30)
fig2.show()

# Initialisierung dritter Konsolenplot für delta_func; Verhältnis von func2 zu func1
fig3 = tpl.figure()
fig3.plot (x_func2, delta_func, label=delta_func, width=50, height=30)
fig3.show()
