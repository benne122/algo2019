import timeit
from fibonacci import fibonacci, fibonacci_mem_wrap

# settings:
set_repeat = 100000
set_range = 12
set_func1 = fibonacci
set_func2 = fibonacci_mem_wrap



# "setup" fuer Timer, hier werden die Funktionen erneut importiert. Dreifache Hochstriche fuer Strings ueber mehrere Zeilen.
setup1 = '''
from '''+set_func1.__name__+''' import '''+set_func1.__name__+'''
'''
setup2 = '''
from '''+set_func1.__name__+''' import '''+set_func2.__name__+'''
''' 

# Schleife fuer die Anzahl "set_range"; berechnet nach und nach von 1 bis set_range die Funktionswerte und gibt sie aus
for i in range (1,set_range):
    
    # Ausgabe aktuelles i
    print ("n: "+str (i))

    # zu timender Code fuer Timer1 in Stringform; set_func.__name__ gibt den set_func1 zugewiesenen Funktionsnamen als String zurueck; Zahlen muessen auch als String vorliegen
    code1 = set_func1.__name__ +"("+str (i)+")"
    
    # Initialisierung erster Timer
    t1 = timeit.Timer (setup=setup1, stmt=code1)
    
    # repeat gibt eine Liste aus in der "set_repeat" viele Messungen enthalten sind. Der minimale Wert ist der aussagekraeftigste, da hier der Prozessor am wenigsten gestoert wurde. Die 1 ist die Anzahl pro repeatvorgang, hier werden allerdings die Zeiten auffaddiert, weswegen 1 als Wert gewaehlt ist
    t1_min = min (t1.repeat (set_repeat, 1))
    
    # Ausgabe des Funktionsnamen plus Funktionswert von "i" aus for Schleife
    print (set_func1.__name__+": "+str (set_func1 (i)))

    # Ausgabe des Funktionsnamen plus minimaler Wert aus repeat. Dies ist die Ausgabe der Messung
    print ("lokale Laufzeit: "+str (t1_min))

    # Wiederholung fuer die zweite Funktion
    code2 = set_func2.__name__ +"("+str (i)+")"
    t2 = timeit.Timer (setup=setup2, stmt=code2)
    t2_min = min (t2.repeat (set_repeat, 1))
    print (set_func2.__name__+": "+str (set_func2 (i)))
    print ("lokale Laufzeit: "+str (t2_min))

    # Ausgabe des Verhaeltnisses der Laufzeit von Funktion 1 zu Funktion 2 mit Abfangen von Laufzeiten, die so klein sind, dass der Wert 0 entspricht; damit Teilung durch 0 verhindert wird
    if (t1_min == 0):
        print ("t2/t1: t1=0")
    else:
        print ("t2/t1: "+str (t2_min/t1_min))
    print ("____________________")
