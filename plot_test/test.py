import termplotlib as tpl
import numpy

x = numpy.linspace (0, 100, 5)
y1 = []
y2 = []

def func1 (x):
    return x*x

def func2 (x):
    return x*x*x

for i in range (100):
    y1.append (func1 (i))
    y2.append (func2 (i))

delta = []
len_y1 = len (y1)
for i in range (len_y1):
    if (y1[i] == 0):
        delta.append (0)
    else:
        delta.append (y2[i]/y1[i])

fig = tpl.figure()
fig.plot (x, delta, label=delta, width=50, height=20)
fig.show()  
