def mergesort (sortingList):

    # Aufteilen bis ein Element übrig
    if (len (sortingList) <= 1):
        return sortingList

    else:        
        # linke Rekursion
        leftPart = mergesort (sortingList[:(len (sortingList) // 2)])
        #print (leftPart)

        # rechte Rekursion
        rightPart = mergesort (sortingList[(len (sortingList) // 2):])
        #print (rightPart)
        
        # Teillisten zusammenführen/zusammensortieren
        mergedList = merge (leftPart, rightPart)
        #print (mergedList)

        return mergedList


def merge (leftPart, rightPart):

    # zusammengeführte Liste
    mergedPart = []
    
    # durchlaufen und entfernen bis eine Liste leer
    while (len (leftPart)>0 and len (rightPart)>0):
        
        if (leftPart[0] <= rightPart[0]):
            mergedPart.append (leftPart.pop (0))
        else:
            mergedPart.append (rightPart.pop (0))

    # übrig gebliebene Elemente anhängen
    while (len (leftPart) > 0):
        mergedPart.append (leftPart.pop (0))
    while (len (rightPart) > 0):
        mergedPart.append (rightPart.pop (0))

    return mergedPart
