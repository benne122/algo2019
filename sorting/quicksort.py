def quicksort (sortingList, variante):
    left = 0
    right = len (sortingList) - 1

    quicksortIntern (sortingList, left, right, variante)



def quicksortIntern (sortingList, left, right, variante):
    
    # aufteilen bis ein Element übrig bleibt
    if (right-left > 0):
        
        # finde Teilungspunkt
        if (variante == "single"):
            pivotIndex = partitioniere1 (sortingList, left, right)
        elif (variante == "dual"):
            pivotIndex = partitioniere2 (sortingList, left, right)
        elif (variante == "single random"):
            return
        else:
            print ("Bitte Art der Partitionierung wählen")
            return 
        
        # linke Rekursion
        quicksortIntern (sortingList, left, pivotIndex-1, variante)

        # rechte Rekursion
        quicksortIntern (sortingList, pivotIndex+1, right, variante)


# Partitionierung nach Blunck und Tutorial
# https://homepages.bluffton.edu/~nesterd/java/SortingDemo.html
def partitioniere1 (sortingList, left, right):

    pivot = sortingList[right]

    split = left

    for j in range (left, right):
        if (sortingList[j] < pivot):
            sortingList[split], sortingList[j] = sortingList[j], sortingList[split]
            split = split+1

    sortingList[split], sortingList[right] = sortingList[right], sortingList[split]
    
    return split
    

# Partitionierung nach Wikipedia
# https://de.wikipedia.org/wiki/Quicksort
def partitioniere2 (sortingList, left, right):

    pivot = sortingList[right]
    
    greater = left
    lower = right-1

    while (True):

        while (sortingList[greater] < pivot and greater < right):
            greater = greater+1

        while (sortingList[lower] >= pivot and lower > left):
            lower = lower-1
        
        if (greater >= lower):
            break
        sortingList[greater], sortingList[lower] = sortingList[lower], sortingList[greater]

    sortingList[greater], sortingList[right] = sortingList[right], sortingList[greater]

    return greater
