"""
 Desc:
  Generate a list of n random integers using randint()
"""
def randList (n):
    from random import shuffle
    
    myList = list (range (n))

    shuffle (myList)

    return myList
